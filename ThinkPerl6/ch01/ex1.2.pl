say "1. How many seconds are there in 42 minutes, 42 seconds?";
my $seconds = (42 * 60) + 42;
say "\n   Answer: There are $seconds seconds, of course.\n";

say "2. How many miles are there in 10 kilometers?";
my $miles = 10 / 1.61;
say "\n   Answer: Why there are $miles miles, obviously.\n";

say "3. If you run a 10 kilometer race in 42 minutes, 42 seconds, what is your";
say "   average pace (time per mile in minutes and seconds)?";
my $seconds-per-mile = $seconds / $miles;
my $minutes = Int($seconds-per-mile) div 60;
$seconds = Int($seconds-per-mile) mod 60;
say "\n   Answer: You silly person, your pace is about $minutes minutes and";
say "           $seconds seconds per mile.";
