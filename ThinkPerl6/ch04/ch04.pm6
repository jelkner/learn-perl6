unit module ch04;


sub days-hours-minutes-seconds(Int $time is copy) is export {
    my $seconds = $time % 60;
    $time div= 60;
    my $minutes = $time % 60;
    $time div= 60;
    my $hours = $time % 24;
    my $days = $time div 24;
    return <$days $hours $minutes $seconds>
};
