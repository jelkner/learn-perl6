unit module tools;

sub avg($n1, $n2) is export {
    return ($n1 + $n2) / 2;
}
