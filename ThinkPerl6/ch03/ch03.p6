use Test;

plan 2;
ok right-justify('Larry Wall'),
'                                                            Larry Wall';
ok right-justify('Ni!'),
'                                                                   Ni!';

sub right-justify(Str $s is copy) {
    my $len = 70 - $s.chars;
    ' ' x $len ~ $s; 
}
