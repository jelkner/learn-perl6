class Game is export {
    my $current_frame = 1;
    my @frames = ((), (), (), (), (), (), (), (), (), ());

    method roll($pins) {
        if @frames[$current_frame-1].elems % 2 == 0 {
            @frames[$current_frame-1] = ($pins,);
        } else {
            @frames[$current_frame-1] = (@frames[$current_frame-1][0], $pins);
            $current_frame++;
        }
    }

    method score() {
        my $total = 0;
        my $frame_total;

        loop (my $i = 0; $i <= 5; $i++) {
            $frame_total = sum(@frames[$i]);
            if $frame_total == 10 {
                $total += 10 + @frames[$i+1][0]; 
            } else {
                $total += $frame_total; 
            }
        }

        return $total;
    }
}
