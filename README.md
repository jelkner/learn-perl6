# Learning Perl6

## Setup Process on Ubuntu 18.04 

1. Run the following at a bash shell:

       $ sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 379CE192D401AB61
       $ echo "deb https://dl.bintray.com/nxadm/rakudo-pkg-debs `lsb_release -cs` main" | sudo tee -a /etc/apt/sources.list.d/rakudo-pkg.list
       $ sudo apt-get update && sudo apt-get install rakudo-pkg
       $ sudo apt install libssl1.0-dev

2. Install Perl6:

       $ sudo apt install perl6

3. Add Perl6 to your path:

       $ /opt/rakudo-pkg/bin/add-perl6-to-path 

Starting the REPL with `$ perl6` then suggest installing Readline with:

    $ zef install Readline


## Installing Cro

    $ zef install --/test cro

> *Note:* I tried running: as instructed on the
> [Cro website](https://cro.services), but it failed on not finding libssl,
> which was remedied with the ``$ sudo apt install libssl1.0-dev`` added to
> the setup process instructions above, after which cro installed without
> complaint.


## Resources

* [The Perl 6 Programming Language](https://perl6.org)
* [rakudo-pkg](https://github.com/nxadm/rakudo-pkg)
* [Cro](https://cro.services)
* [Python to Perl 6 - nutshell](https://docs.perl6.org/language/py-nutshell)
